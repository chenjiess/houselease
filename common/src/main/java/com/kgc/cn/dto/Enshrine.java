package com.kgc.cn.dto;

public class Enshrine {
    private Integer id;

    private Integer userId;

    private String houseId;

    private String enshrineTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getEnshrineTime() {
        return enshrineTime;
    }

    public void setEnshrineTime(String enshrineTime) {
        this.enshrineTime = enshrineTime;
    }
}