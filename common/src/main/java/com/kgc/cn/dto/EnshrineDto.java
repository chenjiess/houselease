package com.kgc.cn.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by Ding on 2020/1/9.
 */
public class EnshrineDto implements Serializable {
    private static final long serialVersionUID = -140692932811949518L;
    private String houseId;
    private String houseAddress;
    private String houseName;
    private Integer leaseMoney;
    private String userId;
    private Integer isJointRent;
    private String bedRoomType;
    private String apartment;

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getHouseAddress() {
        return houseAddress;
    }

    public void setHouseAddress(String houseAddress) {
        this.houseAddress = houseAddress;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public Integer getLeaseMoney() {
        return leaseMoney;
    }

    public void setLeaseMoney(Integer leaseMoney) {
        this.leaseMoney = leaseMoney;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getIsJointRent() {
        return isJointRent;
    }

    public void setIsJointRent(Integer isJointRent) {
        this.isJointRent = isJointRent;
    }

    public String getBedRoomType() {
        return bedRoomType;
    }

    public void setBedRoomType(String bedRoomType) {
        this.bedRoomType = bedRoomType;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getEnshrineTime() {
        return enshrineTime;
    }

    public void setEnshrineTime(String enshrineTime) {
        this.enshrineTime = enshrineTime;
    }

    private String enshrineTime;
}
