package com.kgc.cn.dto;

public class Lesses {
    private Integer id;

    private String lessType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLessType() {
        return lessType;
    }

    public void setLessType(String lessType) {
        this.lessType = lessType;
    }
}