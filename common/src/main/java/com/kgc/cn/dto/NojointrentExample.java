package com.kgc.cn.dto;

import java.util.ArrayList;
import java.util.List;

public class NojointrentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public NojointrentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andHouseIdIsNull() {
            addCriterion("houseId is null");
            return (Criteria) this;
        }

        public Criteria andHouseIdIsNotNull() {
            addCriterion("houseId is not null");
            return (Criteria) this;
        }

        public Criteria andHouseIdEqualTo(String value) {
            addCriterion("houseId =", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotEqualTo(String value) {
            addCriterion("houseId <>", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdGreaterThan(String value) {
            addCriterion("houseId >", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdGreaterThanOrEqualTo(String value) {
            addCriterion("houseId >=", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdLessThan(String value) {
            addCriterion("houseId <", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdLessThanOrEqualTo(String value) {
            addCriterion("houseId <=", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdLike(String value) {
            addCriterion("houseId like", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotLike(String value) {
            addCriterion("houseId not like", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdIn(List<String> values) {
            addCriterion("houseId in", values, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotIn(List<String> values) {
            addCriterion("houseId not in", values, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdBetween(String value1, String value2) {
            addCriterion("houseId between", value1, value2, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotBetween(String value1, String value2) {
            addCriterion("houseId not between", value1, value2, "houseId");
            return (Criteria) this;
        }

        public Criteria andHallMoneyIsNull() {
            addCriterion("hallMoney is null");
            return (Criteria) this;
        }

        public Criteria andHallMoneyIsNotNull() {
            addCriterion("hallMoney is not null");
            return (Criteria) this;
        }

        public Criteria andHallMoneyEqualTo(Integer value) {
            addCriterion("hallMoney =", value, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHallMoneyNotEqualTo(Integer value) {
            addCriterion("hallMoney <>", value, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHallMoneyGreaterThan(Integer value) {
            addCriterion("hallMoney >", value, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHallMoneyGreaterThanOrEqualTo(Integer value) {
            addCriterion("hallMoney >=", value, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHallMoneyLessThan(Integer value) {
            addCriterion("hallMoney <", value, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHallMoneyLessThanOrEqualTo(Integer value) {
            addCriterion("hallMoney <=", value, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHallMoneyIn(List<Integer> values) {
            addCriterion("hallMoney in", values, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHallMoneyNotIn(List<Integer> values) {
            addCriterion("hallMoney not in", values, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHallMoneyBetween(Integer value1, Integer value2) {
            addCriterion("hallMoney between", value1, value2, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHallMoneyNotBetween(Integer value1, Integer value2) {
            addCriterion("hallMoney not between", value1, value2, "hallMoney");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationIsNull() {
            addCriterion("houseOrientation is null");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationIsNotNull() {
            addCriterion("houseOrientation is not null");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationEqualTo(String value) {
            addCriterion("houseOrientation =", value, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationNotEqualTo(String value) {
            addCriterion("houseOrientation <>", value, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationGreaterThan(String value) {
            addCriterion("houseOrientation >", value, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationGreaterThanOrEqualTo(String value) {
            addCriterion("houseOrientation >=", value, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationLessThan(String value) {
            addCriterion("houseOrientation <", value, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationLessThanOrEqualTo(String value) {
            addCriterion("houseOrientation <=", value, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationLike(String value) {
            addCriterion("houseOrientation like", value, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationNotLike(String value) {
            addCriterion("houseOrientation not like", value, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationIn(List<String> values) {
            addCriterion("houseOrientation in", values, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationNotIn(List<String> values) {
            addCriterion("houseOrientation not in", values, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationBetween(String value1, String value2) {
            addCriterion("houseOrientation between", value1, value2, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andHouseOrientationNotBetween(String value1, String value2) {
            addCriterion("houseOrientation not between", value1, value2, "houseOrientation");
            return (Criteria) this;
        }

        public Criteria andParkingLotIsNull() {
            addCriterion("parkingLot is null");
            return (Criteria) this;
        }

        public Criteria andParkingLotIsNotNull() {
            addCriterion("parkingLot is not null");
            return (Criteria) this;
        }

        public Criteria andParkingLotEqualTo(String value) {
            addCriterion("parkingLot =", value, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotNotEqualTo(String value) {
            addCriterion("parkingLot <>", value, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotGreaterThan(String value) {
            addCriterion("parkingLot >", value, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotGreaterThanOrEqualTo(String value) {
            addCriterion("parkingLot >=", value, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotLessThan(String value) {
            addCriterion("parkingLot <", value, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotLessThanOrEqualTo(String value) {
            addCriterion("parkingLot <=", value, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotLike(String value) {
            addCriterion("parkingLot like", value, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotNotLike(String value) {
            addCriterion("parkingLot not like", value, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotIn(List<String> values) {
            addCriterion("parkingLot in", values, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotNotIn(List<String> values) {
            addCriterion("parkingLot not in", values, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotBetween(String value1, String value2) {
            addCriterion("parkingLot between", value1, value2, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andParkingLotNotBetween(String value1, String value2) {
            addCriterion("parkingLot not between", value1, value2, "parkingLot");
            return (Criteria) this;
        }

        public Criteria andIsElevatorIsNull() {
            addCriterion("isElevator is null");
            return (Criteria) this;
        }

        public Criteria andIsElevatorIsNotNull() {
            addCriterion("isElevator is not null");
            return (Criteria) this;
        }

        public Criteria andIsElevatorEqualTo(Integer value) {
            addCriterion("isElevator =", value, "isElevator");
            return (Criteria) this;
        }

        public Criteria andIsElevatorNotEqualTo(Integer value) {
            addCriterion("isElevator <>", value, "isElevator");
            return (Criteria) this;
        }

        public Criteria andIsElevatorGreaterThan(Integer value) {
            addCriterion("isElevator >", value, "isElevator");
            return (Criteria) this;
        }

        public Criteria andIsElevatorGreaterThanOrEqualTo(Integer value) {
            addCriterion("isElevator >=", value, "isElevator");
            return (Criteria) this;
        }

        public Criteria andIsElevatorLessThan(Integer value) {
            addCriterion("isElevator <", value, "isElevator");
            return (Criteria) this;
        }

        public Criteria andIsElevatorLessThanOrEqualTo(Integer value) {
            addCriterion("isElevator <=", value, "isElevator");
            return (Criteria) this;
        }

        public Criteria andIsElevatorIn(List<Integer> values) {
            addCriterion("isElevator in", values, "isElevator");
            return (Criteria) this;
        }

        public Criteria andIsElevatorNotIn(List<Integer> values) {
            addCriterion("isElevator not in", values, "isElevator");
            return (Criteria) this;
        }

        public Criteria andIsElevatorBetween(Integer value1, Integer value2) {
            addCriterion("isElevator between", value1, value2, "isElevator");
            return (Criteria) this;
        }

        public Criteria andIsElevatorNotBetween(Integer value1, Integer value2) {
            addCriterion("isElevator not between", value1, value2, "isElevator");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}