package com.kgc.cn.dto;

import java.io.Serializable;

public class Pictures implements Serializable {
    private static final long serialVersionUID = 7112344481382230188L;
    private Integer id;

    private String pictureUrl;

    private String pictureUri;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getPictureUri() {
        return pictureUri;
    }

    public void setPictureUri(String pictureUri) {
        this.pictureUri = pictureUri;
    }
}