package com.kgc.cn.dto;

public class Rells {
    private Integer id;

    private String rellType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRellType() {
        return rellType;
    }

    public void setRellType(String rellType) {
        this.rellType = rellType;
    }
}