package com.kgc.cn.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @auther zhouxinyu
 * @data 2020/1/8
 */
@Data
public class fuzzyHouseDto implements Serializable {

    private String houseAddress;

    private Integer leaseMoney;

    private Integer isJointRent;

    private String houseBenefit;

    private String leaseRequire;
    private String apartment;
    private String Orientation;

}
