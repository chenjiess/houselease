package com.kgc.cn.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class houseLeaseIsJointDto implements Serializable {
    private static final long serialVersionUID = -2210188602380697501L;
    private String houseId;
    private String houseAddress;
    private String houseName;
    private String houseArea;
    private Integer floor;
    private Integer decorateSituationId;
    private Integer leaseId;
    private Integer leaseMoney;
    private String linkmanName;
    private Integer linkmanSex;
    private Integer userId;
    private String lookHouseTime;
    private Integer leaseHouseNumber;
    private String leaseStartTime;
    private String homeAppliances;
    private String houseBenefit;
    private String leaseRequire;
    private String comment;
    private Integer isJointRent;

    //一下为合租的租房信息
    private String apartment;
    private String bedroomType;
    private String bedroomOrientation;
}
