package com.kgc.cn.enums;

/**
 * Created by Ding on 2020/1/7.
 */
public enum UserEnum {
    LOGIN_ERROR(501, "手机号或密码不正确!"),
    SHOW_ENSHRINE(502,"暂无收藏");
    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    UserEnum() {
    }

    UserEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
