package com.kgc.cn.controller;

import com.kgc.cn.dto.*;
import com.kgc.cn.service.Impl.ShowLeaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@RestController
@RequestMapping("/show")
public class ShowLeaseController {
    @Autowired
    private ShowLeaseServiceImpl showLeaseService;

    @PostMapping("showLease")
    public ShowLeaseDto showLease(@RequestParam("houseId") String houseId) {
        ShowLeaseDto showLeaseDto = showLeaseService.showLease(houseId);
        if (!ObjectUtils.isEmpty(showLeaseDto)) {
            return showLeaseDto;
        }
        return null;
    }

    /**
     * 查所有共同租房信息
     *
     * @return
     */
    @GetMapping(value = "/houseLease")
    public List<Houselease> houseLease(@RequestParam(value = "current") int current,
                                       @RequestParam(value = "size") int size) {
        return showLeaseService.houseLease(current, size);
    }

    /**
     * 查所有合租信息
     *
     * @return
     */

    @GetMapping(value = "/isjointrent")
    public List<Isjointrent> isjointrent() {
        return showLeaseService.isJoin();
    }

    /**
     * 查所有整租信息
     *
     * @return
     */

    @GetMapping(value = "/nojointrent")
    public List<Nojointrent> nojointrent() {
        return showLeaseService.noJoin();
    }

    /**
     * 根据地区查共同信息
     *
     * @param houseAddress
     * @return
     */

    @PostMapping(value = "/byhouseAddress")
    public List<Houselease> byhousearea(@RequestParam(value = "houseArea") String houseAddress,
                                        @RequestParam(value = "current") int current,
                                        @RequestParam(value = "size") int size) {
        return showLeaseService.houseLeaseByAddress(houseAddress, current, size);
    }

    /**
     * 查找所有租房个数
     *
     * @return
     */
    @GetMapping(value = "/houseLeaseNum")
    public int houseLeaseNum() {
        return showLeaseService.houseLeaseNum();
    }

    /**
     * 根据小区地址和小区名称模糊搜索
     *
     * @param search
     * @return
     */
    @PostMapping(value = "/searchByAddressAndName")
    public List<Houselease> searchByAddressAndName(@RequestParam(value = "search") String search,
                                                   @RequestParam(value = "current") int current,
                                                   @RequestParam(value = "size") int size) {
        return showLeaseService.searchByAddAndName(search, current, size);
    }

    /**
     * 查询用户下所有的租赁信息
     *
     * @param userId
     * @return
     */
    @PostMapping(value = "/showLeaseByUserId")
    public List<ShowLeaseDto> showLeaseByUserId(@RequestParam("userId") String userId) {
        return showLeaseService.showLeaseByUserId(userId);
    }

    /**
     * 模糊搜索
     * @param fuzzyHouseDto
     * @param sort
     * @return
     */
    @PostMapping(value = "/fuzzySearch")
    public List<ShowLeaseDto> fuzzySearch(@RequestBody fuzzyHouseDto fuzzyHouseDto,
                                          @RequestParam(value = "sort") int sort){
        return showLeaseService.fuzzySearch(fuzzyHouseDto,sort);
    }

    /**
     * 获得轮播图
     * @return
     */
    @PostMapping("/getPicture")
    public List<Pictures> getPicture(){
        return showLeaseService.getPicture();
    }
    /**
     * 返回地区列表
     * @return
     */

    @GetMapping(value = "/addressList")
    public List<Addresses> addresseList(){
        return showLeaseService.addressList();
    }

    /**
     * 返回价格列表
     * @return
     */
    @GetMapping(value = "/rellList")
    public List<Rells> rellList(){
        return showLeaseService.rellsList();
    }

    /**
     * 返回户型列表
     * @return
     */
    @GetMapping(value = "/bedroomsList")
    public List<Bedrooms> bedroomsList(){
        return showLeaseService.bedroomsList();
    }
    /**
     * 返回朝向列表
     * @return
     */
    @GetMapping(value = "/orientationsList")
    public List<Orientations> orientationsList(){
        return showLeaseService.orientationsList();
    }
    /**
     * 返回出租要求列表
     * @return
     */
    @GetMapping(value = "/leaserequiresList")
    public List<Leaserequires> leaserequiresList(){
        return showLeaseService.leaserequiresList();
    }
    /**
     * 返回房源特色列表
     * @return
     */
    @GetMapping(value = "/housebenefitsList")
    public List<Housebenefits> housebenefitsList(){
        return showLeaseService.housebenefitsList();
    }


}
