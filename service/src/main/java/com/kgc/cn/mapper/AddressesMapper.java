package com.kgc.cn.mapper;

import com.kgc.cn.dto.Addresses;
import com.kgc.cn.dto.AddressesExample;
import java.util.List;

import com.kgc.cn.dto.Decoratesituation;
import org.apache.ibatis.annotations.Param;

public interface AddressesMapper {
    long countByExample(AddressesExample example);

    int deleteByExample(AddressesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Addresses record);

    int insertSelective(Addresses record);

    List<Addresses> selectByExample(AddressesExample example);

    Addresses selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Addresses record, @Param("example") AddressesExample example);

    int updateByExample(@Param("record") Addresses record, @Param("example") AddressesExample example);

    int updateByPrimaryKeySelective(Addresses record);

    int updateByPrimaryKey(Addresses record);

    // 查询所有
    List<Addresses> selectAll();
}