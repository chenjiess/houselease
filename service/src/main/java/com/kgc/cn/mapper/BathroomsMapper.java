package com.kgc.cn.mapper;

import com.kgc.cn.dto.Bathrooms;
import com.kgc.cn.dto.BathroomsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BathroomsMapper {
    long countByExample(BathroomsExample example);

    int deleteByExample(BathroomsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Bathrooms record);

    int insertSelective(Bathrooms record);

    List<Bathrooms> selectByExample(BathroomsExample example);

    Bathrooms selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Bathrooms record, @Param("example") BathroomsExample example);

    int updateByExample(@Param("record") Bathrooms record, @Param("example") BathroomsExample example);

    int updateByPrimaryKeySelective(Bathrooms record);

    int updateByPrimaryKey(Bathrooms record);
}