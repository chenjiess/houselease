package com.kgc.cn.mapper;

import com.kgc.cn.dto.Bedroomtypes;
import com.kgc.cn.dto.BedroomtypesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BedroomtypesMapper {
    long countByExample(BedroomtypesExample example);

    int deleteByExample(BedroomtypesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Bedroomtypes record);

    int insertSelective(Bedroomtypes record);

    List<Bedroomtypes> selectByExample(BedroomtypesExample example);

    Bedroomtypes selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Bedroomtypes record, @Param("example") BedroomtypesExample example);

    int updateByExample(@Param("record") Bedroomtypes record, @Param("example") BedroomtypesExample example);

    int updateByPrimaryKeySelective(Bedroomtypes record);

    int updateByPrimaryKey(Bedroomtypes record);
}