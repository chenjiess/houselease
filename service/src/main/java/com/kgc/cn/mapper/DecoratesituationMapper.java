package com.kgc.cn.mapper;

import com.kgc.cn.dto.Decoratesituation;
import com.kgc.cn.dto.DecoratesituationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DecoratesituationMapper {
    long countByExample(DecoratesituationExample example);

    int deleteByExample(DecoratesituationExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Decoratesituation record);

    int insertSelective(Decoratesituation record);

    List<Decoratesituation> selectByExample(DecoratesituationExample example);

    Decoratesituation selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Decoratesituation record, @Param("example") DecoratesituationExample example);

    int updateByExample(@Param("record") Decoratesituation record, @Param("example") DecoratesituationExample example);

    int updateByPrimaryKeySelective(Decoratesituation record);

    int updateByPrimaryKey(Decoratesituation record);
    //地区查找
    List<Decoratesituation> querydecorateSituation();
}