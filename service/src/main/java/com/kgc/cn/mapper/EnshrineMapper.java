package com.kgc.cn.mapper;

import com.kgc.cn.dto.Enshrine;
import com.kgc.cn.dto.EnshrineDto;
import com.kgc.cn.dto.EnshrineExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EnshrineMapper {
    long countByExample(EnshrineExample example);

    int deleteByExample(EnshrineExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Enshrine record);

    int insertSelective(Enshrine record);

    List<Enshrine> selectByExample(EnshrineExample example);

    Enshrine selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Enshrine record, @Param("example") EnshrineExample example);

    int updateByExample(@Param("record") Enshrine record, @Param("example") EnshrineExample example);

    int updateByPrimaryKeySelective(Enshrine record);

    int updateByPrimaryKey(Enshrine record);

    //展示收藏
    List<EnshrineDto> showEnshrine(@Param("userId") String userId);
}