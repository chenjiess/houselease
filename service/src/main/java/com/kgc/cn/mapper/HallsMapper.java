package com.kgc.cn.mapper;

import com.kgc.cn.dto.Halls;
import com.kgc.cn.dto.HallsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HallsMapper {
    long countByExample(HallsExample example);

    int deleteByExample(HallsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Halls record);

    int insertSelective(Halls record);

    List<Halls> selectByExample(HallsExample example);

    Halls selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Halls record, @Param("example") HallsExample example);

    int updateByExample(@Param("record") Halls record, @Param("example") HallsExample example);

    int updateByPrimaryKeySelective(Halls record);

    int updateByPrimaryKey(Halls record);
}