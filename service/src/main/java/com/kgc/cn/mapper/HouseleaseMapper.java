package com.kgc.cn.mapper;

import com.kgc.cn.dto.Houselease;
import com.kgc.cn.dto.HouseleaseExample;
import com.kgc.cn.dto.ShowLeaseDto;
import com.kgc.cn.dto.fuzzyHouseDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HouseleaseMapper {
    long countByExample(HouseleaseExample example);

    int deleteByExample(HouseleaseExample example);

    int deleteByPrimaryKey(String houseId);

    int insert(Houselease record);

    int insertSelective(Houselease record);

    List<Houselease> selectByExample(HouseleaseExample example);

    Houselease selectByPrimaryKey(String houseId);

    int updateByExampleSelective(@Param("record") Houselease record, @Param("example") HouseleaseExample example);

    int updateByExample(@Param("record") Houselease record, @Param("example") HouseleaseExample example);

    int updateByPrimaryKeySelective(Houselease record);

    int updateByPrimaryKey(Houselease record);

    // 首页展示租房列表(查找出所有租房信息)
    List<Houselease> selectAll(@Param("start") int start, @Param("size") int size);
    //通过房屋id查询房屋出租信息
    ShowLeaseDto showLease(@Param("houseId") String houseId);
    // 首页展示租房列表(查找出地区所有租房信息)
    List<Houselease> selectAllByAddress(@Param("houseAddress") String houseAddress, @Param("start") int start, @Param("size") int size);

    // 查找所有租房信息条数
    int selectAllNum();

    //根据小区地址和小区名称模糊搜索
    List<Houselease> searchByAddAndName(@Param("search") String search, @Param("start") int start, @Param("size") int size);

    //查询用户下所有租赁信息
    List<ShowLeaseDto> showLeaseByUserId(@Param("userId") String userId);

    // 模糊搜索（houselease）
    List<ShowLeaseDto> fuzzySearch(@Param("fuzzyHouseDto") fuzzyHouseDto fuzzyHouseDto,@Param("sort")int sort);

    //houselease
    int insertLease(Houselease houselease);
}