package com.kgc.cn.mapper;

import com.kgc.cn.dto.Isjointrent;
import com.kgc.cn.dto.IsjointrentExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IsjointrentMapper {
    long countByExample(IsjointrentExample example);

    int deleteByExample(IsjointrentExample example);

    int deleteByPrimaryKey(String houseId);

    int insert(Isjointrent record);

    int insertSelective(Isjointrent record);

    List<Isjointrent> selectByExample(IsjointrentExample example);

    Isjointrent selectByPrimaryKey(String houseId);

    int updateByExampleSelective(@Param("record") Isjointrent record, @Param("example") IsjointrentExample example);

    int updateByExample(@Param("record") Isjointrent record, @Param("example") IsjointrentExample example);

    int updateByPrimaryKeySelective(Isjointrent record);

    int updateByPrimaryKey(Isjointrent record);

    // 查找所有
    List<Isjointrent> selectAll();

    //isjointrent
    int insertIsJointRent(Isjointrent isjointrent);
}