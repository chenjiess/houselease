package com.kgc.cn.mapper;

import com.kgc.cn.dto.Nojointrent;
import com.kgc.cn.dto.NojointrentExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NojointrentMapper {
    long countByExample(NojointrentExample example);

    int deleteByExample(NojointrentExample example);

    int deleteByPrimaryKey(String houseId);

    int insert(Nojointrent record);

    int insertSelective(Nojointrent record);

    List<Nojointrent> selectByExample(NojointrentExample example);

    Nojointrent selectByPrimaryKey(String houseId);

    int updateByExampleSelective(@Param("record") Nojointrent record, @Param("example") NojointrentExample example);

    int updateByExample(@Param("record") Nojointrent record, @Param("example") NojointrentExample example);

    int updateByPrimaryKeySelective(Nojointrent record);

    int updateByPrimaryKey(Nojointrent record);

    //查找所有
    List<Nojointrent> selectAll();
}