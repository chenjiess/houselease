package com.kgc.cn.mapper;

import com.kgc.cn.dto.Addresses;
import com.kgc.cn.dto.Rells;
import com.kgc.cn.dto.RellsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RellsMapper {
    long countByExample(RellsExample example);

    int deleteByExample(RellsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Rells record);

    int insertSelective(Rells record);

    List<Rells> selectByExample(RellsExample example);

    Rells selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Rells record, @Param("example") RellsExample example);

    int updateByExample(@Param("record") Rells record, @Param("example") RellsExample example);

    int updateByPrimaryKeySelective(Rells record);

    int updateByPrimaryKey(Rells record);
    // 查询所有
    List<Rells> selectAll();
}