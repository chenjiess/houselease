package com.kgc.cn.service.Impl;

import com.kgc.cn.dto.EnshrineDto;
import com.kgc.cn.mapper.EnshrineMapper;
import com.kgc.cn.service.EnshrineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Ding on 2020/1/9.
 */
@Service
public class EnshrineServiceImpl implements EnshrineService {
    @Autowired
    private EnshrineMapper enshrineMapper;
    @Override
    public List<EnshrineDto> showEnshrine(String userId) {
        return enshrineMapper.showEnshrine(userId);
    }
}
