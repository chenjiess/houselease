package com.kgc.cn.service.Impl;

import com.kgc.cn.dto.*;
import com.kgc.cn.mapper.*;
import com.kgc.cn.mapper.HouseleaseMapper;
import com.kgc.cn.mapper.IsjointrentMapper;
import com.kgc.cn.mapper.NojointrentMapper;
import com.kgc.cn.mapper.PicturesMapper;
import com.kgc.cn.service.ShowLeaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@Service
public class ShowLeaseServiceImpl implements ShowLeaseService {
    @Autowired
    private HouseleaseMapper houseleaseMapper;
    @Autowired
    private IsjointrentMapper isjointrentMapper;
    @Autowired
    private NojointrentMapper nojointrentMapper;
    @Autowired
    private PicturesMapper picturesMapper;

    @Override
    public ShowLeaseDto showLease(String houseId) {
        return houseleaseMapper.showLease(houseId);
    }


    /**
     * 查房租共同信息
     *
     * @return
     */
    @Override
    public List<Houselease> houseLease(int current, int size) {
        int start = (current - 1) * size;
        return houseleaseMapper.selectAll(start, size);
    }

    /**
     * 查找所有租房个数
     *
     * @return
     */
    @Override
    public int houseLeaseNum() {
        return houseleaseMapper.selectAllNum();
    }

    /**
     * 查租房合租信息
     *
     * @return
     */

    @Override
    public List<Isjointrent> isJoin() {
        return isjointrentMapper.selectAll();
    }

    /**
     * 查租房整租信息
     *
     * @return
     */
    @Override
    public List<Nojointrent> noJoin() {
        return nojointrentMapper.selectAll();
    }

    /**
     * 根据地区返回租房信息
     *
     * @param houseAddress
     * @param current
     * @param size
     * @return
     */
    @Override
    public List<Houselease> houseLeaseByAddress(String houseAddress, int current, int size) {
        int start = (current - 1) * size;
        return houseleaseMapper.selectAllByAddress(houseAddress, start, size);
    }

    /**
     * 根据小区地址和小区名称模糊搜索
     *
     * @param search
     * @return
     */

    @Override
    public List<Houselease> searchByAddAndName(String search, int current, int size) {
        int start = (current - 1) * size;
        return houseleaseMapper.searchByAddAndName(search, start, size);
    }


    /**
     * 查询用户下所有的租赁信息
     *
     * @param userId
     * @return
     */
    public List<ShowLeaseDto> showLeaseByUserId(String userId) {
        return houseleaseMapper.showLeaseByUserId(userId);
    }

    /**
     * 模糊查询
     * @param fuzzyHouseDto
     * @return
     */
    @Override
    public List<ShowLeaseDto> fuzzySearch(fuzzyHouseDto fuzzyHouseDto,int sort) {
        return houseleaseMapper.fuzzySearch(fuzzyHouseDto,sort);
    }


    @Autowired
    private AddressesMapper addressesMapper;

    /**
     * 返回地区列表
     *
     * @return
     */
    @Override
    public List<Addresses> addressList() {
        return addressesMapper.selectAll();
    }
    @Autowired
    private RellsMapper rellsMapper;

    /**
     * 返回价格列表
     *
     * @return
     */

    @Override
    public List<Rells> rellsList() {
        return rellsMapper.selectAll();
    }
    @Autowired
    private BedroomsMapper bedroomsMapper;

    /**
     * 返回户型列表（多少室）
     * @return
     */

    @Override
    public List<Bedrooms> bedroomsList() {
        return bedroomsMapper.bedroomsList();
    }

    @Autowired
    private OrientationsMapper orientationsMapper;

    /**
     * 返回朝向列表
     * @return
     */
    @Override
    public List<Orientations> orientationsList() {
        return orientationsMapper.orientationsList();
    }


    @Autowired
    private LeaserequiresMapper leaserequiresMapper;

    /**
     * 返回租房要求列表
     * @return
     */

    @Override
    public List<Leaserequires> leaserequiresList() {
        return leaserequiresMapper.leaserequiresList();
    }
    @Autowired
    private HousebenefitsMapper housebenefitsMapper;

    /**
     * 返回房源特色列表
     * @return
     */

    @Override
    public List<Housebenefits> housebenefitsList() {
        return housebenefitsMapper.housebenefitsList();
    }

    /**
     * 获得轮播图
     * @return
     */
    @Override
    public List<Pictures> getPicture() {
        return picturesMapper.selectAll();
    }
}
