package com.kgc.cn.service.Impl;

import com.kgc.cn.dto.User;
import com.kgc.cn.dto.UserExample;
import com.kgc.cn.mapper.UserMapper;
import com.kgc.cn.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUser(String phone) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andPhoneEqualTo(phone);
        List<User> userList = userMapper.selectByExample(userExample);
        if (!CollectionUtils.isEmpty(userList)) {
            User user = userList.get(0);
            return user;
        }
        return null;
    }
}
