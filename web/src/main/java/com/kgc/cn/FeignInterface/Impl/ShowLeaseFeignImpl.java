package com.kgc.cn.FeignInterface.Impl;

import com.kgc.cn.FeignInterface.ShowLeaseFeignInterface;
import com.kgc.cn.dto.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@Component
public class ShowLeaseFeignImpl implements ShowLeaseFeignInterface {
    @Override
    public ShowLeaseDto showLease(String houseId) {
        return null;
    }

    @Override
    public List<Houselease> houseLease(int current, int size) {
        return null;
    }

    @Override
    public List<Isjointrent> isjointrent() {
        return null;
    }

    @Override
    public List<Nojointrent> nojointrent() {
        return null;
    }

    @Override
    public List<Houselease> byhouseaddress(String houseAddress, int current, int size) {
        return null;
    }

    @Override
    public int houseLeaseNum() {
        return 0;
    }

    @Override
    public List<Houselease> searchByAddressAndName(String search, int current, int size) {
        return null;
    }

    @Override
    public List<ShowLeaseDto> showLeaseByUserId(String userId) {
        return null;
    }

    @Override
    public List<ShowLeaseDto> fuzzySearch(fuzzyHouseDto fuzzyHouseDto,int sort) {
        return null;
    }

    @Override
    public List<Pictures> getPicture() {
        return null;
    }

    @Override
    public List<Addresses> addressList() {
        return null;
    }

    @Override
    public List<Rells> rellList() {
        return null;
    }

    @Override
    public List<Bedrooms> bedroomsList() {
        return null;
    }

    @Override
    public List<Orientations> orientationsList() {
        return null;
    }

    @Override
    public List<Leaserequires> leaserequiresList() {
        return null;
    }

    @Override
    public List<Housebenefits> housebenefitsList() {
        return null;
    }


}
