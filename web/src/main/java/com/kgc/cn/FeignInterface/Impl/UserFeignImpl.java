package com.kgc.cn.FeignInterface.Impl;

import com.kgc.cn.FeignInterface.UserFeignInterface;
import com.kgc.cn.dto.EnshrineDto;
import com.kgc.cn.dto.User;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@Component
public class UserFeignImpl implements UserFeignInterface {

    @Override
    public User getUser(String phone) {
        return null;
    }

    @Override
    public List<EnshrineDto> showEnshrine(String userId) {
        return null;
    }


}
