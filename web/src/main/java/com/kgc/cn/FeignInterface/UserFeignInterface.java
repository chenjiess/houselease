package com.kgc.cn.FeignInterface;

import com.kgc.cn.FeignInterface.Impl.UserFeignImpl;
import com.kgc.cn.dto.EnshrineDto;
import com.kgc.cn.dto.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@FeignClient(name = "service", fallback = UserFeignImpl.class)
public interface UserFeignInterface {
    @PostMapping("/user/getUser")
    User getUser(@RequestParam("phone") String phone);

    @PostMapping("/enshrine/showEnshrine")
    List<EnshrineDto> showEnshrine(@RequestParam("userId") String userId);
}
