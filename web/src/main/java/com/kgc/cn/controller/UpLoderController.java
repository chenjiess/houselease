package com.kgc.cn.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * Created by Ding on 2020/1/9.
 */
@Api(tags = "图片上传")
@RestController
@RequestMapping("/uploder")
public class UpLoderController {
    @ApiOperation("图片上传")
    @PostMapping(path = "/savePhoto")
    public String addDish(@RequestPart("photos") MultipartFile file, HttpServletRequest request) throws Exception {
        String path = null;// 文件路径
        double fileSize = file.getSize();
        System.out.println("文件的大小是" + fileSize);
        byte[] sizebyte = file.getBytes();
        System.out.println("文件的byte大小是" + sizebyte.toString());
        if (file != null) {// 判断上传的文件是否为空
            String type = null;// 文件类型
            String fileName = file.getOriginalFilename();// 文件原名称
            System.out.println("上传的文件原名称:" + fileName);
            // 判断文件类型
            type = fileName.indexOf(".") != -1 ? fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()) : null;
            if (type != null) {// 判断文件类型是否为空
                if ("GIF".equals(type.toUpperCase()) || "PNG".equals(type.toUpperCase()) || "JPG".equals(type.toUpperCase())) {
                    // 项目在容器中实际发布运行的根路径
                    String realPath = request.getSession().getServletContext().getRealPath("/");
                    // 自定义的文件名称
                    String trueFileName = String.valueOf(System.currentTimeMillis()) + "." + type;
                    // 设置存放图片文件的路径
                    path = "E:\\IDEA_work\\houselease\\web\\src\\main\\java\\com\\kgc\\cn\\pictures\\" + fileName;
                    System.out.println("存放图片文件的路径:" + path);
                    // 转存文件到指定的路径
                    file.transferTo(new File(path));
                    return "文件成功上传到指定目录下";
                }
            } else {
                return "不是我们想要的文件类型,请按要求重新上传";
            }
        } else {
            return "文件类型为空";
        }
        return "已经成功上传到指定目录";
    }

}
