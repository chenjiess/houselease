package com.kgc.cn.param;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;

/**
 * @auther zhouxinyu
 * @data 2020/1/9
 */
@Data
@ApiOperation(value = "户型model")
public class BedRoomsParam implements Serializable {
    @ApiModelProperty(value = "户型id",example = "1")
    private Integer id;
    @ApiModelProperty(value = "户型")
    private String bedRoom;
}
