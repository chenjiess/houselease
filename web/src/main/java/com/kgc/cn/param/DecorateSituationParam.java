package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "装修model")
public class DecorateSituationParam implements Serializable {

    private static final long serialVersionUID = 1516681748205611346L;
    @ApiModelProperty(value = "装修状态")
    private String name;
}
