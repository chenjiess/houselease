package com.kgc.cn.param;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;

/**
 * @auther zhouxinyu
 * @data 2020/1/9
 */
@Data
@ApiOperation(value = "租房要求model")
public class LeaserequiresParam implements Serializable {
    @ApiModelProperty(value = "租房要求id",example = "1")
    private Integer id;
    @ApiModelProperty(value = "租房要求")
    private String require;
}
