package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @auther zhouxinyu
 * @data 2020/1/9
 */
@Data
@ApiModel(value = "根据id返回列表model")
public class ListByTypeIdParam {
    @ApiModelProperty(value = "地区列表")
    List<AddressesParam> addressesParamsList;
    @ApiModelProperty(value = "租金列表")
    List<RellsParam> rellsParamsList;
    @ApiModelProperty(value = "出租类型列表")
    List<String> leaseTypeList;
    @ApiModelProperty(value = "户型列表")
    List<BedRoomsParam> bedRoomsParamList;
    @ApiModelProperty(value = "朝向列表")
    List<OrientationsParam> orientationsParamList;
    @ApiModelProperty(value = "出租要求列表")
    List<LeaserequiresParam> leaserequiresParamList;
    @ApiModelProperty(value = "房源特色列表")
    List<HousebenefitsParam> housebenefitsParamList;

}
