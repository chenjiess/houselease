package com.kgc.cn.param;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;

/**
 * @auther zhouxinyu
 * @data 2020/1/9
 */
@Data
@ApiOperation(value = "朝向model")
public class OrientationsParam implements Serializable {
    @ApiModelProperty(value = "朝向表id",example = "1")
    private Integer id;
    @ApiModelProperty(value = "朝向")
    private String orientation;
}
