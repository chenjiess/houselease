package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;

/**
 * @auther zhouxinyu
 * @data 2020/1/9
 */
@Data
@ApiOperation(value = "租金model")
public class RellsParam implements Serializable {
    @ApiModelProperty(value = "租金金额id",example = "1")
    private Integer id;
    @ApiModelProperty(value = "租金金额")
    private String rellType;
}
