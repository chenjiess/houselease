package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "短租model")
public class ShortRentParam implements Serializable {
    private static final long serialVersionUID = 4173626365248194478L;
    @ApiModelProperty(value = "短租支持")
    private String lessType;
}
