package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Ding on 2020/1/8.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户房产model")
public class ShowLeaseByUserParam {
    @ApiModelProperty(value = "房屋id")
    private String houseId;
    @ApiModelProperty("房屋封面")
    private String img;
    @ApiModelProperty("是否合租")
    private String isJR;
    @ApiModelProperty(value = "房屋地址")
    private String houseAddress;
    @ApiModelProperty(value = "小区名称")
    private String houseName;
    @ApiModelProperty(value = "卧室类型")
    private String bedroomType;
    @ApiModelProperty(value = "更新时间")
    private String updateTime;
    @ApiModelProperty(value = "浏览人数",example = "1")
    private Integer showNumber;
}
