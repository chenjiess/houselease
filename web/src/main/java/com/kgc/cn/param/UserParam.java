package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by Ding on 2020/1/7.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户model")
public class UserParam implements Serializable {
    private static final long serialVersionUID = 6765932264061940308L;
    @ApiModelProperty(value = "用户id")
    private String userId;
    @ApiModelProperty(value = "头像地址")
    private String img;
    @ApiModelProperty(value = "昵称")
    private String nickName;
    @ApiModelProperty(value = "用户类型", example = "1")
    private Integer userTypeId;
    @ApiModelProperty(value = "手机号")
    private String phone;
    @ApiModelProperty(value = "密码")
    private String passWord;
}
