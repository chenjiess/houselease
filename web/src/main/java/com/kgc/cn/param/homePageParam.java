package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @auther zhouxinyu
 * @data 2020/1/7
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "主页展示租房信息")
public class homePageParam {
    @ApiModelProperty(value = "标题栏")
    public String firstLine;
    @ApiModelProperty(value = "{厅室}.{合租时，房租类型}.{楼层，高中低}.{建筑面积}.{租金}")
    public String secondLine;
    @ApiModelProperty(value = "地址坐标")
    public String thirdLine;
    @ApiModelProperty(value = "{是否短租}.{装修情况}.{合租显示性别要求}.{房屋亮点}")
    public String forthLine;


}
