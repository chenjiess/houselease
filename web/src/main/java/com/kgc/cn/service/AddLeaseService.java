package com.kgc.cn.service;


import com.kgc.cn.dto.User;
import com.kgc.cn.param.*;
import com.kgc.cn.dto.Addresses;
import com.kgc.cn.dto.Decoratesituation;
import com.kgc.cn.param.*;
import com.kgc.cn.utils.returnResult.ReturnResult;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
public interface AddLeaseService {
    // 添加整租信息
    String addNojoinLease(AddWholeHouseParam wholeHouseParam, User user);

    // 房屋配置展示
    ListParam showMerits();

    // 中途退出保存信息
    String addIncompleteInfo(ShowLeaseParam showLeaseParam, User user);

    // 继续发布信息
    ShowLeaseParam continuePush(User user);

    //合租
    String addRentSharing(RentSharingParam rentSharingParam, UserParam userParam);
    //地区查找
    List<DecorateSituationParam> querydecorateSituation();
    //短租
    List<ShortRentParam> queryShortRent();
}
