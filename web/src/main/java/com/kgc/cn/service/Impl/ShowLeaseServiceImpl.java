package com.kgc.cn.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.kgc.cn.FeignInterface.ShowLeaseFeignInterface;
import com.kgc.cn.dto.*;
import com.kgc.cn.param.*;
import com.kgc.cn.service.ShowLeaseService;
import com.kgc.cn.utils.Date.DateUtils;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ding on 2020/1/7.
 */
@Service
public class ShowLeaseServiceImpl implements ShowLeaseService {
    @Autowired
    private ShowLeaseFeignInterface showLeaseFeignInterface;
    @Autowired
    private RedisUtils redisUtils;
    // 定义redis组名：存放房屋的更新时间
    private String redisGroupUpdateTime = "updateTime:";
    // 定义redis组名：存放房屋的浏览人数
    private String redisGroupBrowse = "browse:";

    /**
     * 获取所有信息
     *
     * @return
     */

    @Override
    public List<homePageParam> homePage(PageLmit pageLmit) {
        List<Houselease> houseleases = houseLease(pageLmit.getPageNum(), pageLmit.getPageSize());
        List<Isjointrent> isjointrents = isjointrent();
        List<Nojointrent> nojointrents = nojointrent();
        return homepageList(houseleases, isjointrents, nojointrents);
    }

    /**
     * 根据地区返回租房信息
     *
     * @return
     */
    @Override
    public List<homePageParam> homePageByhouseAddress(String houseAddress, PageLmit pageLmit) {
        List<Houselease> byhouseareas = byhouseaddress(houseAddress, pageLmit.getPageNum(), pageLmit.getPageSize());
        List<Isjointrent> isjointrents = isjointrent();
        List<Nojointrent> nojointrents = nojointrent();
        return homepageList(byhouseareas, isjointrents, nojointrents);
    }

    /**
     * 通过地名和小区名模糊搜索租房信息
     * @param search
     * @param current
     * @param size
     * @return
     */
    @Override
    public List<homePageParam> homePageByAddressAndName(String search, PageLmit pageLmit) {
        List<Houselease> byAddressAndName = searchByAddressAndName(search, pageLmit);
        List<Isjointrent> isjointrents = isjointrent();
        List<Nojointrent> nojointrents = nojointrent();
        return homepageList(byAddressAndName, isjointrents, nojointrents);
    }

    /**
     * 根据串入的id返回相对应的列表
     * @param id
     * @return
     */
    @Override
    public ListByTypeIdParam ById(int id) {
        ListByTypeIdParam listByTypeIdParam = new ListByTypeIdParam();
        if(id == 1){
            listByTypeIdParam.setAddressesParamsList(addressList());
        } else if(id == 2){
            listByTypeIdParam.setRellsParamsList(rellList());
        } else if(id == 3){
            List<String> leaseType = Lists.newArrayList();
            leaseType.add("不限");
            leaseType.add("整租");
            leaseType.add("合租");
            listByTypeIdParam.setLeaseTypeList(leaseType);
            listByTypeIdParam.setBedRoomsParamList(bedroomsList());
        } else if(id == 4){
            listByTypeIdParam.setOrientationsParamList(orientationsList());
            listByTypeIdParam.setLeaserequiresParamList(leaserequiresList());
            listByTypeIdParam.setHousebenefitsParamList(housebenefitsList());
        } else {
            return null;
        }
        return listByTypeIdParam;
    }


    /**
     * 查所有的共同信息
     *
     * @return
     */
    @Override
    public List<Houselease> houseLease(int current, int size) {
        return showLeaseFeignInterface.houseLease(current, size);
    }

    /**
     * 查所有合租的信息
     *
     * @return
     */
    @Override
    public List<Isjointrent> isjointrent() {
        return showLeaseFeignInterface.isjointrent();
    }

    /**
     * 查所有整租的信息
     *
     * @return
     */
    @Override
    public List<Nojointrent> nojointrent() {
        return showLeaseFeignInterface.nojointrent();
    }

    /**
     * 根据地区返回租房信息
     *
     * @param houseAddress
     * @return
     */
    @Override
    public List<Houselease> byhouseaddress(String houseAddress, int current, int size) {
        return showLeaseFeignInterface.byhouseaddress(houseAddress, current, size);
    }


    /**
     * 整合信息方法
     *
     * @param houseleases
     * @param isjointrents
     * @param nojointrents
     * @return
     */
    @Override
    public List<homePageParam> homepageList(List<Houselease> houseleases, List<Isjointrent> isjointrents, List<Nojointrent> nojointrents) {
        List<homePageParam> list = null;
        list = houseleases.stream().map(houselease -> {
            homePageParam homepageparam = null;
            if (houselease.getIsJointRent() == 0) {
                // 整租
                for (Nojointrent nojointrent : nojointrents) {
                    // 判断楼层的高中低
                    String high = null;
                    int num = houselease.getFloor();
                    if (num >= 1 && num <= 6) {
                        high = "低层";
                    } else if (num >= 7 && num <= 12) {
                        high = "中层";
                    } else if (num >= 12) {
                        high = "高层";
                    }
                    // 构造显示页面
                    if (houselease.getHouseId().equals(nojointrent.getHouseId())) {
                        try {
                            homepageparam = homePageParam.builder()
                                    .firstLine("整租  " + houselease.getHouseAddress() + "  " + houselease.getHouseName())
                                    .secondLine(nojointrent.getHallMoney() + "  " + high + "  " + houselease.getHouseArea() + "   ￥" + houselease.getLeaseMoney())
                                    .thirdLine(houselease.getHouseAddress() + "  " + getUpdateTime(houselease.getHouseId()))
                                    .forthLine(houselease.getName() + "  ")
                                    .build();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else if (houselease.getIsJointRent() == 1) {
                // 合租
                for (Isjointrent isjointrent : isjointrents) {
                    // 判断楼层的高中低
                    String high = null;
                    int num = houselease.getFloor();
                    if (num >= 1 && num <= 6) {
                        high = "低层";
                    } else if (num >= 7 && num <= 12) {
                        high = "中层";
                    } else if (num >= 12) {
                        high = "高层";
                    }
                    // 构造显示页面
                    if (houselease.getHouseId().equals(isjointrent.getHouseId())) {
                        homepageparam = homePageParam.builder()
                                .firstLine("合租  " + houselease.getHouseAddress() + "  " + houselease.getHouseName() + "  " + isjointrent.getBedroomType())
                                .secondLine(isjointrent.getApartment() + "  " + isjointrent.getBedroomType() + "  " + high + "  " + houselease.getHouseArea() + "   ￥" + houselease.getLeaseMoney())
                                .thirdLine(houselease.getHouseAddress())
                                // todo 有瑕疵
                                .forthLine(houselease.getName() + "  ")
                                .build();
                    }
                }
            }
            return homepageparam;
        }).collect(Collectors.toList());
        return list;
    }

    /**
     * 展示租房详细信息
     *
     * @param houseId
     * @return
     */
    @Override
    public ShowLeaseParam showLease(String houseId) {
        ShowLeaseDto showLeaseDto = showLeaseFeignInterface.showLease(houseId);
        if (null != showLeaseDto) {
            ShowLeaseParam showLeaseParam = new ShowLeaseParam();
            BeanUtils.copyProperties(showLeaseDto, showLeaseParam);
            redisUtils.incr(redisGroupBrowse + houseId, 1);
            return showLeaseParam;
        }
        return null;
    }


    /**
     * 分页
     *
     * @param current
     * @param size
     * @param list
     * @return
     */
    @Override
    public PageBeanParam<homePageParam> Page(PageLmit pageLmit, List<homePageParam> list) {
        PageBeanParam<homePageParam> pageBeanParam = PageBeanParam.<homePageParam>builder()
                .count(showLeaseFeignInterface.houseLeaseNum())
                .current(pageLmit.getPageNum())
                .size(pageLmit.getPageSize())
                .list(list)
                .build();
        return pageBeanParam;
    }

    /**
     * 根据小区地址和小区名称模糊搜索
     *
     * @param search
     * @return
     */

    @Override
    public List<Houselease> searchByAddressAndName(String search, PageLmit pageLmit) {
        return showLeaseFeignInterface.searchByAddressAndName(search, pageLmit.getPageNum(), pageLmit.getPageSize());
    }

    /**
     * 查询用户下的租赁信息
     * @param pageLmit
     * @param userId
     * @return
     */
    @Override
    public PageInfo<ShowLeaseByUserParam> showLeaseByUserId(PageLmit pageLmit, String userId) {
        List<ShowLeaseDto> showLeaseDtoList = showLeaseFeignInterface.showLeaseByUserId(userId);
        List<ShowLeaseByUserParam> userLeaseList = Lists.newArrayList();
        showLeaseDtoList.forEach(showLeaseDto -> {
            ShowLeaseByUserParam userLease = new ShowLeaseByUserParam();
            BeanUtils.copyProperties(showLeaseDto, userLease);
            try {
                userLease.setUpdateTime(getUpdateTime(userLease.getHouseId()));
                int showNum = getBrowseNumber(userLease.getHouseId());
                if (showNum != 0) {
                    userLease.setShowNumber(showNum);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (showLeaseDto.getIsJointRent() == 0) {
                userLease.setIsJR("合租");
            } else {
                userLease.setIsJR("整租");
            }

            userLeaseList.add(userLease);
        });
        PageHelper.startPage(pageLmit.getPageNum(), pageLmit.getPageSize());
        PageInfo<ShowLeaseByUserParam> pageInfo = new PageInfo<>(userLeaseList, 5);
        return pageInfo;
    }

    /**
     * 根据房屋id获取更新时间
     *
     * @param houseId
     * @return
     */
    @Override
    public String getUpdateTime(String houseId) throws ParseException {
        Long updateTime = (Long) redisUtils.get(redisGroupUpdateTime + houseId);
        Long nowDate = new Date().getTime();
        //获得时间差
        int days = DateUtils.getTimeDifferenceByDay(updateTime, nowDate);
        int hours = DateUtils.getTimeDifferenceByHour(updateTime, nowDate);
        int minutes = DateUtils.getTimeDifferenceByMinutes(updateTime, nowDate);
        if (hours < 1) {
            return minutes + "分钟前更新";
        } else if (hours < 24) {
            return hours + "小时前更新";
        } else if (days < 3) {
            return days + "天前更新";
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");
            return sdf.format(updateTime);
        }
    }

    /**
     * 根据房屋id获取浏览人数
     *
     * @param houseId
     * @return
     */
    @Override
    public int getBrowseNumber(String houseId) {
        Object object = redisUtils.get(redisGroupBrowse + houseId);
        if (null != object) {
            int browseNumber = (int) object;
            return browseNumber;
        }
        return 0;
    }

    /**
     *  模糊搜索
     *
     * @param fuzzyHouseParam
     * @return
     */
    @Override
    public List<homePageParam> fuzzySearch(FuzzyHouseParam fuzzyHouseParam,int sort) {
        fuzzyHouseDto fuzzyHouseDto = new fuzzyHouseDto();
        BeanUtils.copyProperties(fuzzyHouseParam,fuzzyHouseDto);
        List<ShowLeaseDto> showLeaselist = showLeaseFeignInterface.fuzzySearch(fuzzyHouseDto,sort);
        List<Houselease> houseLeaselist = showLeaselist.stream().map(showLeaseDto -> {
            Houselease houselease = new Houselease();
            BeanUtils.copyProperties(showLeaseDto,houselease);
            return houselease;
        }).collect(Collectors.toList());
        List<Nojointrent> noJoinList = showLeaselist.stream().map(showLeaseDto -> {
            Nojointrent nojointrent = new Nojointrent();
            BeanUtils.copyProperties(showLeaseDto,nojointrent);
            return nojointrent;
        }).collect(Collectors.toList());
        List<Isjointrent> isJoinList = showLeaselist.stream().map(showLeaseDto -> {
            Isjointrent isjointrent = new Isjointrent();
            BeanUtils.copyProperties(showLeaseDto,isjointrent);
            return isjointrent;
        }).collect(Collectors.toList());
        return homepageList(houseLeaselist,isJoinList,noJoinList);
    }

    /**
     * 返回地区列表
     * @return
     */

    @Override
    public List<AddressesParam> addressList() {
        List<AddressesParam> addressesParamList = showLeaseFeignInterface.addressList()
                .stream().map(addresses -> {
                    AddressesParam addressesParam = new AddressesParam();
                    BeanUtils.copyProperties(addresses,addressesParam);
                    return addressesParam;
                }).collect(Collectors.toList());
        return addressesParamList;
    }

    /**
     * 返回价格列表
     * @return
     */

    @Override
    public List<RellsParam> rellList() {
        List<RellsParam> list = showLeaseFeignInterface.rellList()
                .stream().map(rells -> {
                    RellsParam rellsParam = new RellsParam();
                    BeanUtils.copyProperties(rells,rellsParam);
                    return rellsParam;
                }).collect(Collectors.toList());
        return list;
    }

    /**
     * 返回户型列表
     *
     * @return
     */
    @Override
    public List<BedRoomsParam> bedroomsList() {
        List<BedRoomsParam> bedroomslist = showLeaseFeignInterface.bedroomsList()
                .stream().map(bedrooms -> {
                    BedRoomsParam bedRoomsParam = new BedRoomsParam();
                    BeanUtils.copyProperties(bedrooms,bedRoomsParam);
                    return bedRoomsParam;
                }).collect(Collectors.toList());
        return bedroomslist;
    }

    @Override
    public List<OrientationsParam> orientationsList() {
        List<OrientationsParam> orientationsParamList = showLeaseFeignInterface.orientationsList()
                .stream().map(orientations -> {
                    OrientationsParam orientationsParam = new OrientationsParam();
                    BeanUtils.copyProperties(orientations,orientationsParam);
                    return orientationsParam;
                }).collect(Collectors.toList());
        return orientationsParamList;
    }

    @Override
    public List<LeaserequiresParam> leaserequiresList() {
        List<LeaserequiresParam> LeaserequiresParamList = showLeaseFeignInterface.leaserequiresList()
                .stream().map(leaserequires -> {
                    LeaserequiresParam leaserequiresParam = new LeaserequiresParam();
                    BeanUtils.copyProperties(leaserequires,leaserequiresParam);
                    return leaserequiresParam;
                }).collect(Collectors.toList());
        return LeaserequiresParamList;
    }

    @Override
    public List<HousebenefitsParam> housebenefitsList() {
        List<HousebenefitsParam> housebenefitsParamList = showLeaseFeignInterface.housebenefitsList()
                .stream().map(housebenefits -> {
                    HousebenefitsParam housebenefitsParam = new HousebenefitsParam();
                    BeanUtils.copyProperties(housebenefits,housebenefitsParam);
                    return housebenefitsParam;
                }).collect(Collectors.toList());
        return housebenefitsParamList;
    }

    /**
     * 获取轮播图
     * @return
     */
    @Override
    public List<PictureParam> getPicture() {
        List<Pictures> picturesList = showLeaseFeignInterface.getPicture();
        List<PictureParam> pictureParamList = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(picturesList)){
            picturesList.forEach(picture -> {
                PictureParam pictureParam = new PictureParam();
                BeanUtils.copyProperties(picture,pictureParam);
                pictureParamList.add(pictureParam);
            });
            return pictureParamList;
        }
        return null;
    }


}
