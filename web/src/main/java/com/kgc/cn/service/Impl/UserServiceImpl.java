package com.kgc.cn.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.FeignInterface.UserFeignInterface;
import com.kgc.cn.dto.EnshrineDto;
import com.kgc.cn.dto.User;
import com.kgc.cn.param.EnshrineParam;
import com.kgc.cn.service.UserService;
import com.kgc.cn.utils.redis.RedisUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ding on 2020/1/7.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserFeignInterface userFeignInterface;
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 用户登录
     *
     * @param phone
     * @param passWord
     * @param request
     * @return
     */
    @Override
    public String login(String phone, String passWord, HttpServletRequest request) {
        User user = userFeignInterface.getUser(phone);
        if ((!ObjectUtils.isEmpty(user)) && passWord.equals(user.getPassWord())) {
            String sessionId = request.getSession().getId();
            redisUtils.set(sessionId, JSONObject.toJSONString(user));
            return sessionId;
        }
        return "false";
    }

    @Override
    public List<EnshrineParam> showEnshrine(String userId) {
        List<EnshrineDto> enshrineDtoList = userFeignInterface.showEnshrine(userId);
        if (!CollectionUtils.isEmpty(enshrineDtoList)) {
            List<EnshrineParam> enshrineParamList = enshrineDtoList.stream().map(enshrineDto -> {
                EnshrineParam enshrineParam = new EnshrineParam();
                BeanUtils.copyProperties(enshrineDto, enshrineParam);
                return enshrineParam;
            }).collect(Collectors.toList());
            return enshrineParamList;
        }
        return null;
    }
}
