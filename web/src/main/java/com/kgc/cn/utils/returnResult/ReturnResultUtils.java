package com.kgc.cn.utils.returnResult;

import com.kgc.cn.enums.AddEnum;
import com.kgc.cn.enums.ShowEnum;
import com.kgc.cn.enums.UserEnum;

/***
 *
 * 统一返回工具类
 */
public class ReturnResultUtils {

    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage("success");
        return returnResult;
    }

    /***
     * 成功 带信息
     * @return
     */
    public static ReturnResult returnSuccess(String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage(message);
        return returnResult;
    }

    /***
     * 成功 带数据
     * @return
     */
    public static ReturnResult returnSuccess(Object data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMessage("success");
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }

    /***
     * 成功 带数据 带信息
     * @return
     */
    public static ReturnResult returnSuccess(Object data, String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMessage(message);
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }

    /***
     * 失败
     * @return
     */
    public static ReturnResult returnFail(Integer code, String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMessage(message);
        return returnResult;
    }

    /***
     * 失败,使用loginEnum
     * @return
     */
    public static ReturnResult returnFail(UserEnum e) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(e.getCode());
        returnResult.setMessage(e.getMsg());
        return returnResult;
    }

    /***
     * 失败,使用ShowEnum
     * @return
     */
    public static ReturnResult returnFail(ShowEnum e) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(e.getCode());
        returnResult.setMessage(e.getMsg());
        return returnResult;
    }

    /***
     * 失败,使用AddEnum
     * @return
     */
    public static ReturnResult returnFail(AddEnum e) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(e.getCode());
        returnResult.setMessage(e.getMsg());
        return returnResult;
    }

}
